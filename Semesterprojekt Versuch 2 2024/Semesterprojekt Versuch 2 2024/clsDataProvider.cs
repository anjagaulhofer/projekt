﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Drawing;

namespace Semesterprojekt_Versuch_2_2024
{
    internal class clsDataProvider
    {
        const string connectionstring = "datasource= 127.0.0.1; port=3306; username =root; password=; database=semesterprojekt2024;";

        public static void InsertData(klasseanmeldenregestrieren benutzer)
        {

            MySqlConnection connection = new MySqlConnection(connectionstring);
            try { 
            
            string befehl = ("INSERT INTO tbl_benutzerdaten(benutzername, passwort, farbe) VALUES (@benutzername, @passwort, @farbe)");
            MySqlCommand verbindung = new MySqlCommand(befehl, connection);

            verbindung.Parameters.Add("@benutzername", MySqlDbType.VarChar).Value = benutzer.Benutzername;
            verbindung.Parameters.Add("@passwort", MySqlDbType.VarChar).Value = benutzer.Passwort;
            verbindung.Parameters.Add("@farbe", MySqlDbType.VarChar).Value = "white";
            verbindung.CommandTimeout = 60;




            connection.Open();
                verbindung.ExecuteNonQuery();


            }
            catch
            {
                MessageBox.Show("Es gab Probleme bei der anmeldung, bitte versuchen sie es erneut!");
            }
            finally
            {
                connection.Close();
            }
        }

        public static klasseanmeldenregestrieren SelectData(string benutzername, string passwort)
        {

            klasseanmeldenregestrieren benutzer = new klasseanmeldenregestrieren(0, benutzername, passwort, "white", 0, 0, 0,0);

            MySqlConnection connection = new MySqlConnection(connectionstring);
            string befehl = ("SELECT * FROM tbl_benutzerdaten WHERE benutzername=@benutzername AND passwort=@passwort");

            MySqlCommand verbindung = new MySqlCommand(befehl, connection);
            verbindung.Parameters.Add("@benutzername", MySqlDbType.VarChar).Value = benutzername;
            verbindung.Parameters.Add("@passwort", MySqlDbType.VarChar).Value = passwort;
            try
            {
                int richtig = 0;
                verbindung.CommandTimeout = 60;
                connection.Open();

                MySqlDataReader reader = verbindung.ExecuteReader();
                while (reader.Read())
                {
                    benutzer = new klasseanmeldenregestrieren(Convert.ToInt32(reader[0]), Convert.ToString(reader[1]), Convert.ToString(reader[2]), Convert.ToString(reader[3]), Convert.ToInt32(reader[4]), Convert.ToInt32(reader[5]), Convert.ToInt32(reader[6]), Convert.ToInt32(reader[7])); 
                    if (benutzer.Benutzername == benutzername & benutzer.Passwort == passwort)
                    {

                        richtig = 1;


                    }

                }
                if (richtig != 1)
                {
                    //benutzerliste.Add(benutzer);
                    MessageBox.Show("Dieser Benutzer exestiert leider nicht, bitte versuchen sie es erneut");
                }
            }
            catch
            {

            }
            finally
            {

                connection.Close();

            }
            return benutzer;

        }
        public static void SelectDataanmelden(List<klasseanmeldenregestrieren> benutzerliste)
        {

            MySqlConnection connection = new MySqlConnection(connectionstring);
            string befehl = ("SELECT * FROM tbl_benutzerdaten");
            MySqlCommand verbindung = new MySqlCommand(befehl, connection);
            try
            {

                verbindung.CommandTimeout = 60;
                connection.Open();

                MySqlDataReader reader = verbindung.ExecuteReader();
                while (reader.Read())
                {
                    klasseanmeldenregestrieren benutzer = new klasseanmeldenregestrieren(Convert.ToInt32(reader[0]), Convert.ToString(reader[1]), Convert.ToString(reader[2]), Convert.ToString(reader[3]), Convert.ToInt32(reader[4]), Convert.ToInt32(reader[5]), Convert.ToInt32(reader[6]), Convert.ToInt32(reader[6]));
                    benutzerliste.Add(benutzer);

                }

            }
            catch
            {

            }
            finally
            {
                connection.Close();
            }

        }
        

        public static void InsertFarbe(string farbe,int id)
        {
            MySqlConnection connection = new MySqlConnection(connectionstring);
            try
            {


                string befehl = "UPDATE tbl_benutzerdaten SET farbe = @farbe WHERE id = @id";
                MySqlCommand verbindung = new MySqlCommand(befehl, connection);

                verbindung.Parameters.Add("@farbe", MySqlDbType.VarChar).Value = farbe;
                verbindung.Parameters.Add("@id", MySqlDbType.Int64).Value = id;
                verbindung.CommandTimeout = 60;




                connection.Open();
                verbindung.ExecuteNonQuery();


            }
            catch
            {
                MessageBox.Show("Es gab Probleme, bitte versuchen sie es erneut!");
            }
            finally
            {
                connection.Close();
            }
        }
        public static void Ballspielpunke(int punkte, int id)
        {
            MySqlConnection connection = new MySqlConnection(connectionstring);
            try
            {
                



                string befehl = "UPDATE tbl_benutzerdaten SET punkteballspiel = @punkte WHERE id = @id";
                MySqlCommand verbindung = new MySqlCommand(befehl, connection);

                verbindung.Parameters.Add("@punkte", MySqlDbType.VarChar).Value = punkte;
                verbindung.Parameters.Add("@id", MySqlDbType.Int64).Value = id;
                verbindung.CommandTimeout = 60;




                connection.Open();
                verbindung.ExecuteNonQuery();

              
            }
            catch
            {
                MessageBox.Show("Es gab Probleme, bitte versuchen sie es erneut!");
            }
            finally
            {
                connection.Close();
            }
        }
        public static void Zahlenraten(int versuche, int id)
        {
            MySqlConnection connection = new MySqlConnection(connectionstring);
            try
            {




                string befehl = "UPDATE tbl_benutzerdaten SET punkteballspiel = @punkte WHERE id = @id";
                MySqlCommand verbindung = new MySqlCommand(befehl, connection);

                verbindung.Parameters.Add("@versuche", MySqlDbType.VarChar).Value = versuche;
                verbindung.Parameters.Add("@id", MySqlDbType.Int64).Value = id;
                verbindung.CommandTimeout = 60;




                connection.Open();
                verbindung.ExecuteNonQuery();


            }
            catch
            {
                MessageBox.Show("Es gab Probleme, bitte versuchen sie es erneut!");
            }
            finally
            {
                connection.Close();
            }
        }
        public static void gewinntpunke(int punkte,int gegnerpunkte, int id)
        {
            MySqlConnection connection = new MySqlConnection(connectionstring);
            try
            {
                string befehl = "";

                if (gegnerpunkte > punkte)
                {
                    befehl = "UPDATE tbl_benutzerdaten SET  punktegegner4gewinnt = @gegpunkte WHERE id = @id";
                }
                else if (punkte > gegnerpunkte)
                {
                    befehl = "UPDATE tbl_benutzerdaten SET punkte4gewinnt = @punkte  WHERE id = @id";
                }
              
                MySqlCommand verbindung = new MySqlCommand(befehl, connection);

                verbindung.Parameters.Add("@punkte", MySqlDbType.VarChar).Value = punkte;
                verbindung.Parameters.Add("@gegpunkte", MySqlDbType.VarChar).Value = gegnerpunkte;
                verbindung.Parameters.Add("@id", MySqlDbType.Int64).Value = id;
                verbindung.CommandTimeout = 60;




                connection.Open();
                verbindung.ExecuteNonQuery();

               
            }
            catch
            {
                MessageBox.Show("Es gab Probleme, bitte versuchen sie es erneut!");
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
