﻿namespace Semesterprojekt_Versuch_2_2024
{
    partial class frm_spielauswahl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_4gewinnt = new System.Windows.Forms.Button();
            this.btn_zurueck = new System.Windows.Forms.Button();
            this.btn_ballspiel = new System.Windows.Forms.Button();
            this.lbl_id = new System.Windows.Forms.Label();
            this.btn_farbauswahl = new System.Windows.Forms.Button();
            this.btn_zahlenraten = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_4gewinnt
            // 
            this.btn_4gewinnt.Location = new System.Drawing.Point(229, 32);
            this.btn_4gewinnt.Name = "btn_4gewinnt";
            this.btn_4gewinnt.Size = new System.Drawing.Size(157, 62);
            this.btn_4gewinnt.TabIndex = 5;
            this.btn_4gewinnt.Text = "4 Gewinnt";
            this.btn_4gewinnt.UseVisualStyleBackColor = true;
            this.btn_4gewinnt.Click += new System.EventHandler(this.btn_4gewinnt_Click);
            // 
            // btn_zurueck
            // 
            this.btn_zurueck.Location = new System.Drawing.Point(617, 356);
            this.btn_zurueck.Name = "btn_zurueck";
            this.btn_zurueck.Size = new System.Drawing.Size(157, 62);
            this.btn_zurueck.TabIndex = 4;
            this.btn_zurueck.Text = "zurück";
            this.btn_zurueck.UseVisualStyleBackColor = true;
            this.btn_zurueck.Click += new System.EventHandler(this.btn_zurueck_Click);
            // 
            // btn_ballspiel
            // 
            this.btn_ballspiel.AllowDrop = true;
            this.btn_ballspiel.Location = new System.Drawing.Point(26, 32);
            this.btn_ballspiel.Name = "btn_ballspiel";
            this.btn_ballspiel.Size = new System.Drawing.Size(157, 62);
            this.btn_ballspiel.TabIndex = 3;
            this.btn_ballspiel.Text = "Wo versteckt sich der Ball?";
            this.btn_ballspiel.UseVisualStyleBackColor = true;
            this.btn_ballspiel.Click += new System.EventHandler(this.btn_ballspiel_Click);
            // 
            // lbl_id
            // 
            this.lbl_id.AutoSize = true;
            this.lbl_id.Location = new System.Drawing.Point(400, 217);
            this.lbl_id.Name = "lbl_id";
            this.lbl_id.Size = new System.Drawing.Size(0, 16);
            this.lbl_id.TabIndex = 28;
            // 
            // btn_farbauswahl
            // 
            this.btn_farbauswahl.Location = new System.Drawing.Point(617, 288);
            this.btn_farbauswahl.Name = "btn_farbauswahl";
            this.btn_farbauswahl.Size = new System.Drawing.Size(157, 62);
            this.btn_farbauswahl.TabIndex = 29;
            this.btn_farbauswahl.Text = "Spielfarbe auswählen";
            this.btn_farbauswahl.UseVisualStyleBackColor = true;
            this.btn_farbauswahl.Click += new System.EventHandler(this.btn_farbauswahl_Click);
            // 
            // btn_zahlenraten
            // 
            this.btn_zahlenraten.Location = new System.Drawing.Point(26, 124);
            this.btn_zahlenraten.Name = "btn_zahlenraten";
            this.btn_zahlenraten.Size = new System.Drawing.Size(157, 62);
            this.btn_zahlenraten.TabIndex = 30;
            this.btn_zahlenraten.Text = "Zahlenraten";
            this.btn_zahlenraten.UseVisualStyleBackColor = true;
            this.btn_zahlenraten.Click += new System.EventHandler(this.btn_zahlenraten_Click);
            // 
            // frm_spielauswahl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_zahlenraten);
            this.Controls.Add(this.btn_farbauswahl);
            this.Controls.Add(this.lbl_id);
            this.Controls.Add(this.btn_4gewinnt);
            this.Controls.Add(this.btn_zurueck);
            this.Controls.Add(this.btn_ballspiel);
            this.Name = "frm_spielauswahl";
            this.Text = "frm_spielauswahl";
            this.Load += new System.EventHandler(this.frm_spielauswahl_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_4gewinnt;
        private System.Windows.Forms.Button btn_zurueck;
        private System.Windows.Forms.Button btn_ballspiel;
        private System.Windows.Forms.Label lbl_id;
        private System.Windows.Forms.Button btn_farbauswahl;
        private System.Windows.Forms.Button btn_zahlenraten;
    }
}