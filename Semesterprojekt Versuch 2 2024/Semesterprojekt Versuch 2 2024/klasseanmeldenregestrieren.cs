﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Semesterprojekt_Versuch_2_2024
{
    internal class klasseanmeldenregestrieren
    {
        int m_id;
        string m_benutzername;
        string m_passwort;
        string m_farbe;
        int m_punkteballspiel;
        int m_punkte4gewinnt;
        int m_punktegegner4gewinnt;
        int m_versuche;


        public int Id { get => m_id; set => m_id = value; }
        public string Benutzername { get => m_benutzername; set => m_benutzername = value; }
        public string Passwort { get => m_passwort; set => m_passwort = value; }
        public string Farbe { get => m_farbe; set => m_farbe = value; }
        public int Punkteballspiel { get => m_punkteballspiel; set => m_punkteballspiel = value; }
        public int Punkte4gewinnt { get => m_punkte4gewinnt; set => m_punkte4gewinnt = value; }
        public int Punktegegner4gewinnt { get => m_punktegegner4gewinnt; set => m_punktegegner4gewinnt = value; }
        public int Versuche { get => m_versuche; set => m_versuche = value; }

        public klasseanmeldenregestrieren(int id, string benutzername, string passwort, string farbe, int punkteballspiel, int punkte4gewinnt, int punktegegner4gewinnt, int versuche)
        {
            Id = id;
            Benutzername = benutzername;
            Passwort = passwort;
            m_farbe = farbe;
            m_punkteballspiel = punkteballspiel;
            m_punkte4gewinnt = punkte4gewinnt;
            m_punktegegner4gewinnt = punktegegner4gewinnt;
            
        }
     
        public klasseanmeldenregestrieren(string benutzername, string passwort)
        {
            Benutzername = benutzername;
            Passwort = passwort;
        }

    }
}
