﻿namespace Semesterprojekt_Versuch_2_2024
{
    partial class frm_farbauswahl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_color = new System.Windows.Forms.Button();
            this.cd_color = new System.Windows.Forms.ColorDialog();
            this.cb_farben = new System.Windows.Forms.ComboBox();
            this.btn_farbeuebernehmen = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_color
            // 
            this.btn_color.Location = new System.Drawing.Point(204, 38);
            this.btn_color.Name = "btn_color";
            this.btn_color.Size = new System.Drawing.Size(408, 24);
            this.btn_color.TabIndex = 0;
            this.btn_color.UseVisualStyleBackColor = true;
            this.btn_color.Click += new System.EventHandler(this.btn_color_Click);
            // 
            // cb_farben
            // 
            this.cb_farben.FormattingEnabled = true;
            this.cb_farben.Items.AddRange(new object[] {
            "Blau",
            "Grün",
            "Gelb",
            "Orange",
            "Rot",
            "Rosa",
            "Violet"});
            this.cb_farben.Location = new System.Drawing.Point(12, 38);
            this.cb_farben.Name = "cb_farben";
            this.cb_farben.Size = new System.Drawing.Size(121, 24);
            this.cb_farben.TabIndex = 1;
            this.cb_farben.SelectedIndexChanged += new System.EventHandler(this.cb_farben_SelectedIndexChanged);
            // 
            // btn_farbeuebernehmen
            // 
            this.btn_farbeuebernehmen.Location = new System.Drawing.Point(448, 187);
            this.btn_farbeuebernehmen.Name = "btn_farbeuebernehmen";
            this.btn_farbeuebernehmen.Size = new System.Drawing.Size(164, 23);
            this.btn_farbeuebernehmen.TabIndex = 2;
            this.btn_farbeuebernehmen.Text = "Farbe übernehmen";
            this.btn_farbeuebernehmen.UseVisualStyleBackColor = true;
            this.btn_farbeuebernehmen.Click += new System.EventHandler(this.btn_farbeuebernehmen_Click);
            // 
            // frm_farbauswahl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_farbeuebernehmen);
            this.Controls.Add(this.cb_farben);
            this.Controls.Add(this.btn_color);
            this.Name = "frm_farbauswahl";
            this.Text = "S";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frm_farbauswahl_FormClosed);
            this.Load += new System.EventHandler(this.frm_farbauswahl_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_color;
        private System.Windows.Forms.ColorDialog cd_color;
        private System.Windows.Forms.ComboBox cb_farben;
        private System.Windows.Forms.Button btn_farbeuebernehmen;
    }
}