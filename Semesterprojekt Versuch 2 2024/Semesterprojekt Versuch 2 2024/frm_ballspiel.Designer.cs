﻿namespace Semesterprojekt_Versuch_2_2024
{
    partial class frm_ballspiel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_ziel = new System.Windows.Forms.Label();
            this.btn_neuerunde = new System.Windows.Forms.Button();
            this.lbl_punkte = new System.Windows.Forms.Label();
            this.lbl_punktestand = new System.Windows.Forms.Label();
            this.lbl_frage2 = new System.Windows.Forms.Label();
            this.btn_box2 = new System.Windows.Forms.Button();
            this.btn_box3 = new System.Windows.Forms.Button();
            this.btn_box1 = new System.Windows.Forms.Button();
            this.lbl_zahl3 = new System.Windows.Forms.Label();
            this.lbl_zahl2 = new System.Windows.Forms.Label();
            this.lbl_zahl1 = new System.Windows.Forms.Label();
            this.lbl_frage = new System.Windows.Forms.Label();
            this.btn_schließen = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_ziel
            // 
            this.lbl_ziel.AutoSize = true;
            this.lbl_ziel.Location = new System.Drawing.Point(634, 80);
            this.lbl_ziel.Name = "lbl_ziel";
            this.lbl_ziel.Size = new System.Drawing.Size(97, 16);
            this.lbl_ziel.TabIndex = 35;
            this.lbl_ziel.Text = "Ziel 100 Punkte";
            // 
            // btn_neuerunde
            // 
            this.btn_neuerunde.Location = new System.Drawing.Point(621, 282);
            this.btn_neuerunde.Name = "btn_neuerunde";
            this.btn_neuerunde.Size = new System.Drawing.Size(148, 31);
            this.btn_neuerunde.TabIndex = 34;
            this.btn_neuerunde.Text = "neue Runde";
            this.btn_neuerunde.UseVisualStyleBackColor = true;
            this.btn_neuerunde.Click += new System.EventHandler(this.btn_neuerunde_Click);
            // 
            // lbl_punkte
            // 
            this.lbl_punkte.AutoSize = true;
            this.lbl_punkte.Location = new System.Drawing.Point(165, 110);
            this.lbl_punkte.Name = "lbl_punkte";
            this.lbl_punkte.Size = new System.Drawing.Size(0, 16);
            this.lbl_punkte.TabIndex = 33;
            // 
            // lbl_punktestand
            // 
            this.lbl_punktestand.AutoSize = true;
            this.lbl_punktestand.Location = new System.Drawing.Point(31, 110);
            this.lbl_punktestand.Name = "lbl_punktestand";
            this.lbl_punktestand.Size = new System.Drawing.Size(118, 16);
            this.lbl_punktestand.TabIndex = 32;
            this.lbl_punktestand.Text = "Dein Punktestand: ";
            // 
            // lbl_frage2
            // 
            this.lbl_frage2.AutoSize = true;
            this.lbl_frage2.Location = new System.Drawing.Point(250, 372);
            this.lbl_frage2.Name = "lbl_frage2";
            this.lbl_frage2.Size = new System.Drawing.Size(253, 16);
            this.lbl_frage2.TabIndex = 31;
            this.lbl_frage2.Text = "Klicken Sie bitte auf Ihre Ausgewählte Box";
            // 
            // btn_box2
            // 
            this.btn_box2.Location = new System.Drawing.Point(320, 198);
            this.btn_box2.Name = "btn_box2";
            this.btn_box2.Size = new System.Drawing.Size(98, 56);
            this.btn_box2.TabIndex = 30;
            this.btn_box2.UseVisualStyleBackColor = true;
            this.btn_box2.Click += new System.EventHandler(this.btn_box2_Click_1);
            // 
            // btn_box3
            // 
            this.btn_box3.Location = new System.Drawing.Point(469, 198);
            this.btn_box3.Name = "btn_box3";
            this.btn_box3.Size = new System.Drawing.Size(98, 56);
            this.btn_box3.TabIndex = 29;
            this.btn_box3.UseVisualStyleBackColor = true;
            this.btn_box3.Click += new System.EventHandler(this.btn_box3_Click);
            // 
            // btn_box1
            // 
            this.btn_box1.Location = new System.Drawing.Point(162, 198);
            this.btn_box1.Name = "btn_box1";
            this.btn_box1.Size = new System.Drawing.Size(98, 56);
            this.btn_box1.TabIndex = 28;
            this.btn_box1.UseVisualStyleBackColor = true;
            this.btn_box1.Click += new System.EventHandler(this.btn_box1_Click);
            // 
            // lbl_zahl3
            // 
            this.lbl_zahl3.AutoSize = true;
            this.lbl_zahl3.Location = new System.Drawing.Point(521, 220);
            this.lbl_zahl3.Name = "lbl_zahl3";
            this.lbl_zahl3.Size = new System.Drawing.Size(0, 16);
            this.lbl_zahl3.TabIndex = 27;
            // 
            // lbl_zahl2
            // 
            this.lbl_zahl2.AutoSize = true;
            this.lbl_zahl2.Location = new System.Drawing.Point(377, 220);
            this.lbl_zahl2.Name = "lbl_zahl2";
            this.lbl_zahl2.Size = new System.Drawing.Size(0, 16);
            this.lbl_zahl2.TabIndex = 26;
            // 
            // lbl_zahl1
            // 
            this.lbl_zahl1.AutoSize = true;
            this.lbl_zahl1.Location = new System.Drawing.Point(199, 220);
            this.lbl_zahl1.Name = "lbl_zahl1";
            this.lbl_zahl1.Size = new System.Drawing.Size(0, 16);
            this.lbl_zahl1.TabIndex = 25;
            // 
            // lbl_frage
            // 
            this.lbl_frage.AutoSize = true;
            this.lbl_frage.Location = new System.Drawing.Point(199, 62);
            this.lbl_frage.Name = "lbl_frage";
            this.lbl_frage.Size = new System.Drawing.Size(280, 16);
            this.lbl_frage.TabIndex = 24;
            this.lbl_frage.Text = "Hinter welchem Kästchen befindetsich der Ball";
            // 
            // btn_schließen
            // 
            this.btn_schließen.Location = new System.Drawing.Point(621, 372);
            this.btn_schließen.Name = "btn_schließen";
            this.btn_schließen.Size = new System.Drawing.Size(148, 31);
            this.btn_schließen.TabIndex = 36;
            this.btn_schließen.Text = "Schließen";
            this.btn_schließen.UseVisualStyleBackColor = true;
            this.btn_schließen.Click += new System.EventHandler(this.btn_schließen_Click);
            // 
            // frm_ballspiel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_schließen);
            this.Controls.Add(this.lbl_ziel);
            this.Controls.Add(this.btn_neuerunde);
            this.Controls.Add(this.lbl_punkte);
            this.Controls.Add(this.lbl_punktestand);
            this.Controls.Add(this.lbl_frage2);
            this.Controls.Add(this.btn_box2);
            this.Controls.Add(this.btn_box3);
            this.Controls.Add(this.btn_box1);
            this.Controls.Add(this.lbl_zahl3);
            this.Controls.Add(this.lbl_zahl2);
            this.Controls.Add(this.lbl_zahl1);
            this.Controls.Add(this.lbl_frage);
            this.Name = "frm_ballspiel";
            this.Text = "frm_ballspiel";
            this.Load += new System.EventHandler(this.frm_ballspiel_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_ziel;
        private System.Windows.Forms.Button btn_neuerunde;
        private System.Windows.Forms.Label lbl_punkte;
        private System.Windows.Forms.Label lbl_punktestand;
        private System.Windows.Forms.Label lbl_frage2;
        private System.Windows.Forms.Button btn_box2;
        private System.Windows.Forms.Button btn_box3;
        private System.Windows.Forms.Button btn_box1;
        private System.Windows.Forms.Label lbl_zahl3;
        private System.Windows.Forms.Label lbl_zahl2;
        private System.Windows.Forms.Label lbl_zahl1;
        private System.Windows.Forms.Label lbl_frage;
        private System.Windows.Forms.Button btn_schließen;
    }
}