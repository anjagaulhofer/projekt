﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Semesterprojekt_Versuch_2_2024
{
    public partial class frm_ballspiel : Form
    {
        Random random = new Random();
        int zahl = 0;
        klasseanmeldenregestrieren benutzer;
        public frm_ballspiel(int id, string benutzername, string passwort, string farbe, int punkteball, int punkte4gewinnt, int gegnerpunkte, int versuche)
        {
            InitializeComponent();
            benutzer = new klasseanmeldenregestrieren(id, benutzername, passwort, farbe, punkteball, punkte4gewinnt, gegnerpunkte, versuche);
        }


        private void btn_neuerunde_Click(object sender, EventArgs e)
        {
            
            lbl_punkte.Text = Convert.ToString(benutzer.Punkteballspiel);
            clsDataProvider.Ballspielpunke(benutzer.Punkteballspiel,benutzer.Id);

            zahl = random.Next(1, 3);

            btn_box1.BackColor = Color.White;
            btn_box2.BackColor = Color.White;
            btn_box3.BackColor = Color.White;
        }

        private void frm_ballspiel_Load(object sender, EventArgs e)
        {

            lbl_punkte.Text = Convert.ToString(benutzer.Punkteballspiel);
            clsDataProvider.Ballspielpunke(benutzer.Punkteballspiel, benutzer.Id);

            zahl = random.Next(1, 3);

            btn_box1.BackColor = Color.White;
            btn_box2.BackColor = Color.White;
            btn_box3.BackColor = Color.White;
           
        }


        private void btn_box1_Click(object sender, EventArgs e)
        {
            if (zahl==1)
            {

                btn_box1.BackColor = Color.Green;

                btn_box2.BackColor = Color.Red;
                btn_box3.BackColor = Color.Red;
                benutzer.Punkteballspiel = benutzer.Punkteballspiel + 10;
            }
            else
            {

                btn_box1.BackColor = Color.Red;
                if (zahl==2)
                {
                    btn_box2.BackColor = Color.Green;
                    btn_box3.BackColor = Color.Red;
                }
                else if (zahl==3)
                {
                    btn_box3.BackColor = Color.Green;
                    btn_box2.BackColor = Color.Red;

                }
                benutzer.Punkteballspiel = benutzer.Punkteballspiel - 10;

            }
            lbl_punkte.Text = Convert.ToString(benutzer.Punkteballspiel);

            if (benutzer.Punkteballspiel >= 100)
            {
                MessageBox.Show("Sie haben gewonnen");
            }
            if (benutzer.Punkteballspiel < 1)
            {
                MessageBox.Show("Sie haben verloren");
            }

        }

        private void btn_box2_Click(object sender, EventArgs e)
        {
            if (zahl == 1)
            {

                btn_box1.BackColor = Color.Green;

                btn_box2.BackColor = Color.Red;
                btn_box3.BackColor = Color.Red;
                benutzer.Punkteballspiel = benutzer.Punkteballspiel + 10;
            }
            else
            {

                btn_box1.BackColor = Color.Red;
                if (zahl == 2)
                {
                    btn_box2.BackColor = Color.Green;
                    btn_box3.BackColor = Color.Red;
                }
                else if (zahl == 3)
                {
                    btn_box3.BackColor = Color.Green;
                    btn_box2.BackColor = Color.Red;

                }
                benutzer.Punkteballspiel =    benutzer.Punkteballspiel - 10;

            }
            lbl_punkte.Text = Convert.ToString(benutzer.Punkteballspiel);

            if (benutzer.Punkteballspiel  >= 100)
            {
                MessageBox.Show("Sie haben gewonnen");
            }
            if (benutzer.Punkteballspiel < 0)
            {
                MessageBox.Show("Sie haben verloren");
            }
        }

       
        private void btn_box2_Click_1(object sender, EventArgs e)
        {
            if (zahl == 2)
            {

                btn_box1.BackColor = Color.Red;

                btn_box2.BackColor = Color.Green;
                btn_box3.BackColor = Color.Red;
                benutzer.Punkteballspiel = benutzer.Punkteballspiel + 10;
            }
            else
            {

                btn_box2.BackColor = Color.Red;
                if (zahl == 1)
                {
                    btn_box1.BackColor = Color.Green;
                    btn_box3.BackColor = Color.Red;
                }
                else if (zahl == 3)
                {
                    btn_box3.BackColor = Color.Green;
                    btn_box1.BackColor = Color.Red;

                }
                benutzer.Punkteballspiel = benutzer.Punkteballspiel - 10;

            }
            lbl_punkte.Text = Convert.ToString(benutzer.Punkteballspiel);

            if (benutzer.Punkteballspiel >= 100)
            {
                MessageBox.Show("Sie haben gewonnen");
            }
            if (benutzer.Punkteballspiel < 0)
            {
                MessageBox.Show("Sie haben verloren");
            }

        }

        private void btn_box3_Click(object sender, EventArgs e)
        {
            if (zahl == 3)
            {

                btn_box1.BackColor = Color.Red;

                btn_box2.BackColor = Color.Red;
                btn_box3.BackColor = Color.Green;
                benutzer.Punkteballspiel = benutzer.Punkteballspiel + 10;
            }
            else
            {

                btn_box3.BackColor = Color.Red;
                if (zahl == 1)
                {
                    btn_box1.BackColor = Color.Green;
                    btn_box2.BackColor = Color.Red;
                }
                else if (zahl == 3)
                {
                    btn_box2.BackColor = Color.Green;
                    btn_box1.BackColor = Color.Red;

                }
                benutzer.Punkteballspiel     = benutzer.Punkteballspiel - 10;

            }
            lbl_punkte.Text = Convert.ToString(benutzer.Punkteballspiel);

            if (benutzer.Punkteballspiel >= 100)
            {
                MessageBox.Show("Sie haben gewonnen");
            }
            if (benutzer.Punkteballspiel < 0)
            {
                MessageBox.Show("Sie haben verloren");
            }
        }

        private void btn_schließen_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            clsDataProvider.Ballspielpunke(benutzer.Punkteballspiel, benutzer.Id);
        }
    }
}
