﻿namespace Semesterprojekt_Versuch_2_2024
{
    partial class frm_regestrieren
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_schliesen = new System.Windows.Forms.Button();
            this.lbl_regestrieren = new System.Windows.Forms.Label();
            this.btn_regestieren = new System.Windows.Forms.Button();
            this.txt_passwort = new System.Windows.Forms.TextBox();
            this.txt_benutzername = new System.Windows.Forms.TextBox();
            this.lbl_rpasswort = new System.Windows.Forms.Label();
            this.lbl_rbenutzername = new System.Windows.Forms.Label();
            this.lbl_id = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_schliesen
            // 
            this.btn_schliesen.Location = new System.Drawing.Point(396, 247);
            this.btn_schliesen.Name = "btn_schliesen";
            this.btn_schliesen.Size = new System.Drawing.Size(75, 23);
            this.btn_schliesen.TabIndex = 26;
            this.btn_schliesen.Text = "Schließen";
            this.btn_schliesen.UseVisualStyleBackColor = true;
            this.btn_schliesen.Click += new System.EventHandler(this.btn_schliesen_Click);
            // 
            // lbl_regestrieren
            // 
            this.lbl_regestrieren.AutoSize = true;
            this.lbl_regestrieren.Location = new System.Drawing.Point(209, 89);
            this.lbl_regestrieren.Name = "lbl_regestrieren";
            this.lbl_regestrieren.Size = new System.Drawing.Size(85, 16);
            this.lbl_regestrieren.TabIndex = 25;
            this.lbl_regestrieren.Text = "Regestrieren";
            // 
            // btn_regestieren
            // 
            this.btn_regestieren.Location = new System.Drawing.Point(212, 247);
            this.btn_regestieren.Name = "btn_regestieren";
            this.btn_regestieren.Size = new System.Drawing.Size(94, 23);
            this.btn_regestieren.TabIndex = 24;
            this.btn_regestieren.Text = "Regestrieren";
            this.btn_regestieren.UseVisualStyleBackColor = true;
            this.btn_regestieren.Click += new System.EventHandler(this.btn_regestieren_Click);
            // 
            // txt_passwort
            // 
            this.txt_passwort.Location = new System.Drawing.Point(212, 185);
            this.txt_passwort.Name = "txt_passwort";
            this.txt_passwort.Size = new System.Drawing.Size(207, 22);
            this.txt_passwort.TabIndex = 23;
            // 
            // txt_benutzername
            // 
            this.txt_benutzername.Location = new System.Drawing.Point(212, 132);
            this.txt_benutzername.Name = "txt_benutzername";
            this.txt_benutzername.Size = new System.Drawing.Size(207, 22);
            this.txt_benutzername.TabIndex = 22;
            // 
            // lbl_rpasswort
            // 
            this.lbl_rpasswort.AutoSize = true;
            this.lbl_rpasswort.Location = new System.Drawing.Point(97, 185);
            this.lbl_rpasswort.Name = "lbl_rpasswort";
            this.lbl_rpasswort.Size = new System.Drawing.Size(62, 16);
            this.lbl_rpasswort.TabIndex = 21;
            this.lbl_rpasswort.Text = "Passwort";
            // 
            // lbl_rbenutzername
            // 
            this.lbl_rbenutzername.AutoSize = true;
            this.lbl_rbenutzername.Location = new System.Drawing.Point(97, 135);
            this.lbl_rbenutzername.Name = "lbl_rbenutzername";
            this.lbl_rbenutzername.Size = new System.Drawing.Size(93, 16);
            this.lbl_rbenutzername.TabIndex = 20;
            this.lbl_rbenutzername.Text = "Benutzername";
            // 
            // lbl_id
            // 
            this.lbl_id.AutoSize = true;
            this.lbl_id.Location = new System.Drawing.Point(1, 425);
            this.lbl_id.Name = "lbl_id";
            this.lbl_id.Size = new System.Drawing.Size(0, 16);
            this.lbl_id.TabIndex = 27;
            // 
            // frm_regestrieren
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lbl_id);
            this.Controls.Add(this.btn_schliesen);
            this.Controls.Add(this.lbl_regestrieren);
            this.Controls.Add(this.btn_regestieren);
            this.Controls.Add(this.txt_passwort);
            this.Controls.Add(this.txt_benutzername);
            this.Controls.Add(this.lbl_rpasswort);
            this.Controls.Add(this.lbl_rbenutzername);
            this.Name = "frm_regestrieren";
            this.Text = "Regestrieren";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_schliesen;
        private System.Windows.Forms.Label lbl_regestrieren;
        private System.Windows.Forms.Button btn_regestieren;
        private System.Windows.Forms.TextBox txt_passwort;
        private System.Windows.Forms.TextBox txt_benutzername;
        private System.Windows.Forms.Label lbl_rpasswort;
        private System.Windows.Forms.Label lbl_rbenutzername;
        private System.Windows.Forms.Label lbl_id;
    }
}