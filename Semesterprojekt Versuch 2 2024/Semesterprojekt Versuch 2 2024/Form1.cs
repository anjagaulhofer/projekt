﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Semesterprojekt_Versuch_2_2024
{
    public partial class frm_anmelden : Form
    {
        public frm_anmelden()
        {
            InitializeComponent();
        }

        private void btn_anmelden_Click(object sender, EventArgs e)
        {
         
            List<klasseanmeldenregestrieren> benutzerliste = new List<klasseanmeldenregestrieren>();

            bool funktioniert = true;
            clsDataProvider.SelectDataanmelden(benutzerliste);
            string benutzername = Convert.ToString(txt_benutzername.Text);
            string passwort = Convert.ToString(txt_passwort.Text);
            for (int i = 0; i < benutzerliste.Count; i++)
            {
               

                if (benutzerliste[i].Benutzername != benutzername)
                {
                    funktioniert = true;
                    

                }
                else if (benutzerliste[i].Benutzername == benutzername)
                {

                    funktioniert = false;
                    break;
                }

            }
            if (funktioniert == false)
            {
                txt_benutzername.Text = "";
                txt_passwort.Text = "";
                MessageBox.Show("Dieser Benutzername ist leider schon vergeben. Bitte Regestrieren Sie sich oder wählen Sie einen anderen Benutzernamen");

            }
            else if (funktioniert == true)
            {
                klasseanmeldenregestrieren benutzer = new klasseanmeldenregestrieren(Convert.ToString(txt_benutzername.Text), Convert.ToString(txt_passwort.Text));
                clsDataProvider.InsertData(benutzer);

                txt_benutzername.Text = "";
                txt_passwort.Text = "";
                MessageBox.Show("Sie wurden erfolgreich angemeldet. Bitte Regestrieren Sie sich jetzt");
                frm_regestrieren starten = new frm_regestrieren();


                starten.ShowDialog();
                this.Close();

                DialogResult = DialogResult.OK;
            }



        }

        private void btn_regestrieren_Click(object sender, EventArgs e)
        {
            frm_regestrieren starten = new frm_regestrieren();
            starten.ShowDialog();

        }

        private void btn_schliesen_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
