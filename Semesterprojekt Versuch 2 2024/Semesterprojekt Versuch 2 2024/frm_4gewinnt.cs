﻿using MySqlX.XDevAPI.Relational;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace Semesterprojekt_Versuch_2_2024
{
    public partial class frm_4gewinnt : Form
    {
        klasseanmeldenregestrieren benutzer;

        int x = 0;
      

        int[,] feld = new int[7, 6];
        int[] spalte = new int[7];

        bool rot = true;
        bool vorbei = false;

        int anzahlwuerfe = 0;
       
       
        
        public frm_4gewinnt(int id, string benutzername, string passwort, string farbe, int punkteball, int punkte4gewinnt, int gegnerpunkte,int versuche)
        {
            InitializeComponent();
            benutzer = new klasseanmeldenregestrieren(id, benutzername, passwort, farbe, punkteball, punkte4gewinnt, gegnerpunkte, versuche);
            spielfeld = p_spielfeld.CreateGraphics();
            brush = new SolidBrush(Color.Violet);
            
          
        }

        Graphics spielfeld;
        SolidBrush brush;


        public void neustart()
        {

            Graphics spielfeld = p_spielfeld.CreateGraphics();
            SolidBrush brush = new SolidBrush(Color.Violet);
          
            anzahlwuerfe = 0;// wie viele Steine im Spiel sind
            spielfeld.Clear(Color.Black);//Hintergrund
            brush.Color = Color.White;// Welche Farbe die Löcher haben
            rot = true;
            vorbei = false;


            for (int i = 0; i < 7; i++)// solange Spalte nicht voll ist 7 ZEilen
            {
                spalte[i] = 0;
                for (int j = 0; j < 6; j++)
                {
                    spielfeld.FillEllipse(brush, i * 100 + 10, j * 100 + 10, 80, 80);// Größe und Ort der Weisen Löcher im Spielfeld
                    feld[i, j] = 10;
                }
            }
            int punkte = benutzer.Punkte4gewinnt;
            int gegpunkte = benutzer.Punktegegner4gewinnt;
            

        }



       

        private void btn_neustart_Click(object sender, EventArgs e)
        {
            
            neustart();
        }

        private void btn_zurueck_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void p_spielfeld_MouseDown(object sender, MouseEventArgs e)
        {
            x = e.X / 100;
         


            if (vorbei == false)// noch keiner Gewonnen hat
            {

                if (spalte[x] < 6)
                {
                    


                        if (rot)
                        {

                        if (benutzer.Farbe == "Blau")
                        {

                            brush.Color = Color.Blue;
                        }
                        else if (benutzer.Farbe == "Grün")
                        {
                            brush.Color = Color.Green;
                        }
                        else if (benutzer.Farbe == "Gelb")
                        {
                            brush.Color = Color.Yellow;
                        }
                        else if (benutzer.Farbe == "Orange")
                        {
                            brush.Color = Color.Orange;
                        }
                        else if (benutzer.Farbe == "Rot")
                        {
                            brush.Color = Color.Red;
                        }
                        else if (benutzer.Farbe == "Rosa")
                        {
                            brush.Color = Color.Pink;
                        }
                        else if (benutzer.Farbe == "Violet")
                        {
                            brush.Color = Color.Violet;
                        }
                     
                       
                            feld[x, spalte[x]] = 2;//Für Prüfung später

                     
                        }

                        else
                        {

                        brush.Color = Color.Brown;
                            feld[x, spalte[x]] = 1;//Für Prüfung später

                        }

                        spielfeld.FillEllipse(brush, x * 100 + 10, (5 - spalte[x]) * 100 + 10, 80, 80);
                        spalte[x]++;
                        anzahlwuerfe++;
                        rot = !rot;
                             

                }
                else
                {
                    MessageBox.Show("Spalte schon voll!");

                }
                pruefefeld();
            }
        }
        public void pruefefeld()
        {
            int summe = 0;
            string gewinner = "";



            //Prüfe senkrecht
            for (int x = 0; x < 7; x++)
            {
                for (int y = 0; y < 3; y++)
                {
                    summe = feld[x, y] + feld[x, y + 1] + feld[x, y + 2] + feld[x, y + 3];
                    if (summe == 4)//gelb gewonnen
                    {
                        gewinner = "Ihr Gegner";

                    }
                    if (summe == 8)// rot gewonnen
                    {
                        gewinner = benutzer.Benutzername;
                    }
                }
            }
            //Prüfe waagrecht
            for (int x = 0; x < 4; x++)
            {
                for (int y = 0; y < 6; y++)
                {
                    summe = feld[x, y] + feld[x + 1, y] + feld[x + 2, y] + feld[x + 3, y];
                    if (summe == 4)//gelb gewonnen
                    {
                        gewinner = "Ihr Gegner";

                    }
                    if (summe == 8)// rot gewonnen
                    {
                        gewinner = benutzer.Benutzername;
                    }
                }
            }
            //Prüfe diagonal
            for (int x = 0; x < 4; x++)
            {
                for (int y = 0; y < 3; y++)
                {
                    summe = feld[x, y] + feld[x + 1, y + 1] + feld[x + 2, y + 2] + feld[x + 3, y + 3];
                    if (summe == 4)//gelb gewonnen
                    {
                        gewinner = "Ihr Gegner";

                    }
                    if (summe == 8)// rot gewonnen
                    {
                        gewinner = benutzer.Benutzername;
                    }
                    summe = feld[x, y + 3] + feld[x + 1, y + 2] + feld[x + 2, y + 1] + feld[x + 3, y];
                    if (summe == 4)//gelb gewonnen
                    {
                        gewinner = "Ihr Gegner";

                    }
                    if (summe == 8)// rot gewonnen
                    {
                        gewinner = benutzer.Benutzername;
                    }
                }

            }
            if (anzahlwuerfe == 42 && gewinner == "")
            {
                MessageBox.Show("Unentschieden");
                vorbei = true;
            }

            else if (gewinner != "")
            {
                MessageBox.Show(gewinner + " hat gewonnen");
                int punkte = benutzer.Punkte4gewinnt;
                int gegpunkte = benutzer.Punktegegner4gewinnt;
                if(gewinner=="Ihr Gegner")
                {

                    gegpunkte++;
                    clsDataProvider.gewinntpunke(punkte, gegpunkte, benutzer.Id);
                }
               else if (gewinner==benutzer.Benutzername)
                {
                    punkte++;
                    clsDataProvider.gewinntpunke(punkte, gegpunkte, benutzer.Id);
                }
                vorbei = true;
                DialogResult = DialogResult.OK;
            }

        }

        private void btn_weiter_Paint(object sender, PaintEventArgs e)
        {
            neustart();
        }

        private void btn_farbfeld1_Click(object sender, EventArgs e)
        {

        }

        private void frm_4gewinnt_Load(object sender, EventArgs e)
        {
            MessageBox.Show("Klicken Sie auf Neustart um das Spiel zu starten");
            if (benutzer.Farbe == "Blau")
            {

                btn_farbfeld1.BackColor = Color.Blue;
            }
            else if (benutzer.Farbe == "Grün")
            {
                btn_farbfeld1.BackColor = Color.Green;
            }
            else if (benutzer.Farbe == "Gelb")
            {
                btn_farbfeld1.BackColor = Color.Yellow;
            }
            else if (benutzer.Farbe == "Orange")
            {
                btn_farbfeld1.BackColor = Color.Orange;
            }
            else if (benutzer.Farbe == "Rot")
            {
                btn_farbfeld1.BackColor = Color.Red;
            }
            else if (benutzer.Farbe == "Rosa")
            {
                btn_farbfeld1.BackColor = Color.Pink;
            }
            else if (benutzer.Farbe == "Violet")
            {
                btn_farbfeld1.BackColor = Color.Violet;
            }
           
        }
    }
}
