﻿namespace Semesterprojekt_Versuch_2_2024
{
    partial class frm_4gewinnt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_spielername2 = new System.Windows.Forms.Label();
            this.lbl_spielername1 = new System.Windows.Forms.Label();
            this.btn_zurueck = new System.Windows.Forms.Button();
            this.btn_neustart = new System.Windows.Forms.Button();
            this.p_spielfeld = new System.Windows.Forms.Panel();
            this.btn_farbfeld1 = new System.Windows.Forms.Button();
            this.lbl_farbe = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbl_spielername2
            // 
            this.lbl_spielername2.AutoSize = true;
            this.lbl_spielername2.Location = new System.Drawing.Point(12, 301);
            this.lbl_spielername2.Name = "lbl_spielername2";
            this.lbl_spielername2.Size = new System.Drawing.Size(0, 16);
            this.lbl_spielername2.TabIndex = 22;
            // 
            // lbl_spielername1
            // 
            this.lbl_spielername1.AutoSize = true;
            this.lbl_spielername1.Location = new System.Drawing.Point(12, 177);
            this.lbl_spielername1.Name = "lbl_spielername1";
            this.lbl_spielername1.Size = new System.Drawing.Size(0, 16);
            this.lbl_spielername1.TabIndex = 21;
            // 
            // btn_zurueck
            // 
            this.btn_zurueck.Location = new System.Drawing.Point(12, 100);
            this.btn_zurueck.Name = "btn_zurueck";
            this.btn_zurueck.Size = new System.Drawing.Size(208, 52);
            this.btn_zurueck.TabIndex = 20;
            this.btn_zurueck.Text = "Zurück";
            this.btn_zurueck.UseVisualStyleBackColor = true;
            this.btn_zurueck.Click += new System.EventHandler(this.btn_zurueck_Click);
            // 
            // btn_neustart
            // 
            this.btn_neustart.Location = new System.Drawing.Point(12, 25);
            this.btn_neustart.Name = "btn_neustart";
            this.btn_neustart.Size = new System.Drawing.Size(208, 52);
            this.btn_neustart.TabIndex = 19;
            this.btn_neustart.Text = "Neustart";
            this.btn_neustart.UseVisualStyleBackColor = true;
            this.btn_neustart.Click += new System.EventHandler(this.btn_neustart_Click);
            // 
            // p_spielfeld
            // 
            this.p_spielfeld.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.p_spielfeld.Location = new System.Drawing.Point(226, 12);
            this.p_spielfeld.Name = "p_spielfeld";
            this.p_spielfeld.Size = new System.Drawing.Size(962, 738);
            this.p_spielfeld.TabIndex = 18;
            this.p_spielfeld.MouseDown += new System.Windows.Forms.MouseEventHandler(this.p_spielfeld_MouseDown);
            // 
            // btn_farbfeld1
            // 
            this.btn_farbfeld1.Location = new System.Drawing.Point(12, 257);
            this.btn_farbfeld1.Name = "btn_farbfeld1";
            this.btn_farbfeld1.Size = new System.Drawing.Size(208, 41);
            this.btn_farbfeld1.TabIndex = 23;
            this.btn_farbfeld1.UseVisualStyleBackColor = true;
            this.btn_farbfeld1.Click += new System.EventHandler(this.btn_farbfeld1_Click);
            // 
            // lbl_farbe
            // 
            this.lbl_farbe.AutoSize = true;
            this.lbl_farbe.Location = new System.Drawing.Point(12, 223);
            this.lbl_farbe.Name = "lbl_farbe";
            this.lbl_farbe.Size = new System.Drawing.Size(68, 16);
            this.lbl_farbe.TabIndex = 26;
            this.lbl_farbe.Text = "ihre Farbe";
            // 
            // frm_4gewinnt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1222, 763);
            this.Controls.Add(this.lbl_farbe);
            this.Controls.Add(this.btn_farbfeld1);
            this.Controls.Add(this.lbl_spielername2);
            this.Controls.Add(this.lbl_spielername1);
            this.Controls.Add(this.btn_zurueck);
            this.Controls.Add(this.btn_neustart);
            this.Controls.Add(this.p_spielfeld);
            this.Name = "frm_4gewinnt";
            this.Text = "frm_4gewinnt";
            this.Load += new System.EventHandler(this.frm_4gewinnt_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lbl_spielername2;
        private System.Windows.Forms.Label lbl_spielername1;
        private System.Windows.Forms.Button btn_zurueck;
        private System.Windows.Forms.Button btn_neustart;
        private System.Windows.Forms.Panel p_spielfeld;
        private System.Windows.Forms.Button btn_farbfeld1;
        private System.Windows.Forms.Label lbl_farbe;
    }
}