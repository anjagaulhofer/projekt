﻿namespace Semesterprojekt_Versuch_2_2024
{
    partial class frm_anmelden
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_schliesen = new System.Windows.Forms.Button();
            this.btn_anmelden = new System.Windows.Forms.Button();
            this.txt_passwort = new System.Windows.Forms.TextBox();
            this.txt_benutzername = new System.Windows.Forms.TextBox();
            this.lbl_apasswort = new System.Windows.Forms.Label();
            this.lbl_abenutzername = new System.Windows.Forms.Label();
            this.lbl_anmelden = new System.Windows.Forms.Label();
            this.btn_regestrieren = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_schliesen
            // 
            this.btn_schliesen.Location = new System.Drawing.Point(361, 231);
            this.btn_schliesen.Name = "btn_schliesen";
            this.btn_schliesen.Size = new System.Drawing.Size(75, 23);
            this.btn_schliesen.TabIndex = 26;
            this.btn_schliesen.Text = "Schließen";
            this.btn_schliesen.UseVisualStyleBackColor = true;
            this.btn_schliesen.Click += new System.EventHandler(this.btn_schliesen_Click);
            // 
            // btn_anmelden
            // 
            this.btn_anmelden.Location = new System.Drawing.Point(208, 231);
            this.btn_anmelden.Name = "btn_anmelden";
            this.btn_anmelden.Size = new System.Drawing.Size(109, 23);
            this.btn_anmelden.TabIndex = 25;
            this.btn_anmelden.Text = "Anmelden";
            this.btn_anmelden.UseVisualStyleBackColor = true;
            this.btn_anmelden.Click += new System.EventHandler(this.btn_anmelden_Click);
            // 
            // txt_passwort
            // 
            this.txt_passwort.Location = new System.Drawing.Point(208, 169);
            this.txt_passwort.Name = "txt_passwort";
            this.txt_passwort.Size = new System.Drawing.Size(207, 22);
            this.txt_passwort.TabIndex = 24;
            // 
            // txt_benutzername
            // 
            this.txt_benutzername.Location = new System.Drawing.Point(208, 116);
            this.txt_benutzername.Name = "txt_benutzername";
            this.txt_benutzername.Size = new System.Drawing.Size(207, 22);
            this.txt_benutzername.TabIndex = 23;
            // 
            // lbl_apasswort
            // 
            this.lbl_apasswort.AutoSize = true;
            this.lbl_apasswort.Location = new System.Drawing.Point(93, 169);
            this.lbl_apasswort.Name = "lbl_apasswort";
            this.lbl_apasswort.Size = new System.Drawing.Size(62, 16);
            this.lbl_apasswort.TabIndex = 22;
            this.lbl_apasswort.Text = "Passwort";
            // 
            // lbl_abenutzername
            // 
            this.lbl_abenutzername.AutoSize = true;
            this.lbl_abenutzername.Location = new System.Drawing.Point(93, 119);
            this.lbl_abenutzername.Name = "lbl_abenutzername";
            this.lbl_abenutzername.Size = new System.Drawing.Size(93, 16);
            this.lbl_abenutzername.TabIndex = 21;
            this.lbl_abenutzername.Text = "Benutzername";
            // 
            // lbl_anmelden
            // 
            this.lbl_anmelden.AutoSize = true;
            this.lbl_anmelden.Location = new System.Drawing.Point(205, 73);
            this.lbl_anmelden.Name = "lbl_anmelden";
            this.lbl_anmelden.Size = new System.Drawing.Size(68, 16);
            this.lbl_anmelden.TabIndex = 20;
            this.lbl_anmelden.Text = "Anmelden";
            // 
            // btn_regestrieren
            // 
            this.btn_regestrieren.Location = new System.Drawing.Point(96, 293);
            this.btn_regestrieren.Name = "btn_regestrieren";
            this.btn_regestrieren.Size = new System.Drawing.Size(357, 45);
            this.btn_regestrieren.TabIndex = 27;
            this.btn_regestrieren.Text = "Regestrieren";
            this.btn_regestrieren.UseVisualStyleBackColor = true;
            this.btn_regestrieren.Click += new System.EventHandler(this.btn_regestrieren_Click);
            // 
            // frm_anmelden
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_regestrieren);
            this.Controls.Add(this.btn_schliesen);
            this.Controls.Add(this.btn_anmelden);
            this.Controls.Add(this.txt_passwort);
            this.Controls.Add(this.txt_benutzername);
            this.Controls.Add(this.lbl_apasswort);
            this.Controls.Add(this.lbl_abenutzername);
            this.Controls.Add(this.lbl_anmelden);
            this.Name = "frm_anmelden";
            this.Text = "Anmelden";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_schliesen;
        private System.Windows.Forms.Button btn_anmelden;
        private System.Windows.Forms.TextBox txt_passwort;
        private System.Windows.Forms.TextBox txt_benutzername;
        private System.Windows.Forms.Label lbl_apasswort;
        private System.Windows.Forms.Label lbl_abenutzername;
        private System.Windows.Forms.Label lbl_anmelden;
        private System.Windows.Forms.Button btn_regestrieren;
    }
}

