﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Semesterprojekt_Versuch_2_2024
{
    public partial class frm_spielauswahl : Form
    {
        int iddesspielers = 0;
        klasseanmeldenregestrieren benutzer;
        
        public frm_spielauswahl(int id)
        {
            InitializeComponent();
            iddesspielers = id;

        }
        public frm_spielauswahl(int id,string farbe)
        {
            InitializeComponent();
            iddesspielers = id;

        }
        public frm_spielauswahl(int id, string benutzername, string passwort, string farbe, int punkteball, int punkte4gewinnt, int gegnerpunkte,int versuche)
        {
            InitializeComponent();
            benutzer = new klasseanmeldenregestrieren(id, benutzername, passwort, farbe, punkteball, punkte4gewinnt, gegnerpunkte, versuche);

        }
       
        private void btn_ballspiel_Click(object sender, EventArgs e)
        {
            frm_ballspiel starten = new frm_ballspiel(benutzer.Id, benutzer.Benutzername, benutzer.Passwort, benutzer.Farbe, benutzer.Punkteballspiel, benutzer.Punkte4gewinnt, benutzer.Punktegegner4gewinnt, benutzer.Versuche);
            if (starten.ShowDialog() == DialogResult.OK)
            {
                string benutzername = benutzer.Benutzername;
                string passwort = benutzer.Passwort;
                benutzer = clsDataProvider.SelectData(benutzername, passwort);
            }
        }

        private void btn_zurueck_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_4gewinnt_Click(object sender, EventArgs e)
        {
            frm_4gewinnt starten = new frm_4gewinnt(benutzer.Id, benutzer.Benutzername, benutzer.Passwort, benutzer.Farbe, benutzer.Punkteballspiel, benutzer.Punkte4gewinnt, benutzer.Punktegegner4gewinnt, benutzer.Versuche);
            if (starten.ShowDialog() == DialogResult.OK)
            {
                string benutzername = benutzer.Benutzername;
                string passwort = benutzer.Passwort;
                benutzer = clsDataProvider.SelectData(benutzername, passwort);
            }
        }

        private void frm_spielauswahl_Load(object sender, EventArgs e)
        {
       
        }

        private void btn_farbauswahl_Click(object sender, EventArgs e)
        {
            frm_farbauswahl starten = new frm_farbauswahl(benutzer.Id, benutzer.Benutzername, benutzer.Passwort, benutzer.Farbe, benutzer.Punkteballspiel, benutzer.Punkte4gewinnt, benutzer.Punktegegner4gewinnt, benutzer.Versuche);
            if (starten.ShowDialog() == DialogResult.OK)
            {
                string benutzername = benutzer.Benutzername;
                string passwort = benutzer.Passwort;
                benutzer = clsDataProvider.SelectData(benutzername,passwort);
            }

        }

        private void btn_zahlenraten_Click(object sender, EventArgs e)
        {
            frm_zahlenraten starten = new frm_zahlenraten(benutzer.Id, benutzer.Benutzername, benutzer.Passwort, benutzer.Farbe, benutzer.Punkteballspiel, benutzer.Punkte4gewinnt, benutzer.Punktegegner4gewinnt, benutzer.Versuche);

            if (starten.ShowDialog() == DialogResult.OK)
            {
                string benutzername = benutzer.Benutzername;
                string passwort = benutzer.Passwort;
                benutzer = clsDataProvider.SelectData(benutzername, passwort);
            }
        }

        
    }
}
