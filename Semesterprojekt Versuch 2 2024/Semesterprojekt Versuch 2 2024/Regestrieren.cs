﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Semesterprojekt_Versuch_2_2024
{
    public partial class frm_regestrieren : Form
    {
        public frm_regestrieren()
        {
            InitializeComponent();
        }

        private void btn_regestieren_Click(object sender, EventArgs e)
        {
            klasseanmeldenregestrieren benutzer;
            try
            {
               
                benutzer = clsDataProvider.SelectData(Convert.ToString(txt_benutzername.Text), Convert.ToString(txt_passwort.Text));


                if (benutzer.Id != 0)
                {
                    frm_spielauswahl starten = new frm_spielauswahl(benutzer.Id, benutzer.Benutzername, benutzer.Passwort, benutzer.Farbe, benutzer.Punkteballspiel, benutzer.Punkte4gewinnt, benutzer.Punktegegner4gewinnt,benutzer.Versuche);
                    starten.ShowDialog();
                    this.Close();
                }
            }
            catch
            {

            }
           

        }

        private void btn_schliesen_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
