﻿namespace Semesterprojekt_Versuch_2_2024
{
    partial class frm_zahlenraten
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_zahlenraten = new System.Windows.Forms.Label();
            this.lbl_tipp = new System.Windows.Forms.Label();
            this.lbl_ihreschaetzung = new System.Windows.Forms.Label();
            this.btn_tipp = new System.Windows.Forms.Button();
            this.lbl_versuche = new System.Windows.Forms.Label();
            this.lbl_anzahlversuche = new System.Windows.Forms.Label();
            this.txt_schaetzung = new System.Windows.Forms.TextBox();
            this.btn_einloggen = new System.Windows.Forms.Button();
            this.lbl_bereich = new System.Windows.Forms.Label();
            this.lbl_von10 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbl_zahlenraten
            // 
            this.lbl_zahlenraten.AutoSize = true;
            this.lbl_zahlenraten.Location = new System.Drawing.Point(158, 36);
            this.lbl_zahlenraten.Name = "lbl_zahlenraten";
            this.lbl_zahlenraten.Size = new System.Drawing.Size(78, 16);
            this.lbl_zahlenraten.TabIndex = 0;
            this.lbl_zahlenraten.Text = "Zahlenraten";
            // 
            // lbl_tipp
            // 
            this.lbl_tipp.AutoSize = true;
            this.lbl_tipp.Location = new System.Drawing.Point(37, 311);
            this.lbl_tipp.Name = "lbl_tipp";
            this.lbl_tipp.Size = new System.Drawing.Size(0, 16);
            this.lbl_tipp.TabIndex = 1;
            // 
            // lbl_ihreschaetzung
            // 
            this.lbl_ihreschaetzung.AutoSize = true;
            this.lbl_ihreschaetzung.Location = new System.Drawing.Point(37, 144);
            this.lbl_ihreschaetzung.Name = "lbl_ihreschaetzung";
            this.lbl_ihreschaetzung.Size = new System.Drawing.Size(94, 16);
            this.lbl_ihreschaetzung.TabIndex = 2;
            this.lbl_ihreschaetzung.Text = "Ihre Schätzung";
            // 
            // btn_tipp
            // 
            this.btn_tipp.Location = new System.Drawing.Point(40, 462);
            this.btn_tipp.Name = "btn_tipp";
            this.btn_tipp.Size = new System.Drawing.Size(624, 57);
            this.btn_tipp.TabIndex = 3;
            this.btn_tipp.Text = "Tipp";
            this.btn_tipp.UseVisualStyleBackColor = true;
            this.btn_tipp.Click += new System.EventHandler(this.btn_tipp_Click);
            // 
            // lbl_versuche
            // 
            this.lbl_versuche.AutoSize = true;
            this.lbl_versuche.Location = new System.Drawing.Point(487, 212);
            this.lbl_versuche.Name = "lbl_versuche";
            this.lbl_versuche.Size = new System.Drawing.Size(64, 16);
            this.lbl_versuche.TabIndex = 4;
            this.lbl_versuche.Text = "Versuche";
            // 
            // lbl_anzahlversuche
            // 
            this.lbl_anzahlversuche.AutoSize = true;
            this.lbl_anzahlversuche.Location = new System.Drawing.Point(557, 212);
            this.lbl_anzahlversuche.Name = "lbl_anzahlversuche";
            this.lbl_anzahlversuche.Size = new System.Drawing.Size(0, 16);
            this.lbl_anzahlversuche.TabIndex = 5;
            // 
            // txt_schaetzung
            // 
            this.txt_schaetzung.Location = new System.Drawing.Point(161, 141);
            this.txt_schaetzung.Name = "txt_schaetzung";
            this.txt_schaetzung.Size = new System.Drawing.Size(276, 22);
            this.txt_schaetzung.TabIndex = 6;
            // 
            // btn_einloggen
            // 
            this.btn_einloggen.Location = new System.Drawing.Point(40, 399);
            this.btn_einloggen.Name = "btn_einloggen";
            this.btn_einloggen.Size = new System.Drawing.Size(624, 57);
            this.btn_einloggen.TabIndex = 7;
            this.btn_einloggen.Text = "Antwort einloggen";
            this.btn_einloggen.UseVisualStyleBackColor = true;
            this.btn_einloggen.Click += new System.EventHandler(this.btn_einloggen_Click);
            // 
            // lbl_bereich
            // 
            this.lbl_bereich.AutoSize = true;
            this.lbl_bereich.Location = new System.Drawing.Point(158, 77);
            this.lbl_bereich.Name = "lbl_bereich";
            this.lbl_bereich.Size = new System.Drawing.Size(358, 16);
            this.lbl_bereich.TabIndex = 8;
            this.lbl_bereich.Text = "Der Bereich der zu erratenden Zahl liegt zwischen 0 und 100";
            // 
            // lbl_von10
            // 
            this.lbl_von10.AutoSize = true;
            this.lbl_von10.Location = new System.Drawing.Point(583, 212);
            this.lbl_von10.Name = "lbl_von10";
            this.lbl_von10.Size = new System.Drawing.Size(25, 16);
            this.lbl_von10.TabIndex = 9;
            this.lbl_von10.Text = "/10";
            // 
            // frm_zahlenraten
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(676, 543);
            this.Controls.Add(this.lbl_von10);
            this.Controls.Add(this.lbl_bereich);
            this.Controls.Add(this.btn_einloggen);
            this.Controls.Add(this.txt_schaetzung);
            this.Controls.Add(this.lbl_anzahlversuche);
            this.Controls.Add(this.lbl_versuche);
            this.Controls.Add(this.btn_tipp);
            this.Controls.Add(this.lbl_ihreschaetzung);
            this.Controls.Add(this.lbl_tipp);
            this.Controls.Add(this.lbl_zahlenraten);
            this.Name = "frm_zahlenraten";
            this.Text = "Zahlenraten";
            this.Load += new System.EventHandler(this.frm_zahlenraten_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_zahlenraten;
        private System.Windows.Forms.Label lbl_tipp;
        private System.Windows.Forms.Label lbl_ihreschaetzung;
        private System.Windows.Forms.Button btn_tipp;
        private System.Windows.Forms.Label lbl_versuche;
        private System.Windows.Forms.Label lbl_anzahlversuche;
        private System.Windows.Forms.TextBox txt_schaetzung;
        private System.Windows.Forms.Button btn_einloggen;
        private System.Windows.Forms.Label lbl_bereich;
        private System.Windows.Forms.Label lbl_von10;
    }
}