﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Semesterprojekt_Versuch_2_2024
{
    //Hansi war da 
    public partial class frm_zahlenraten : Form
    {
        string tipp = "";
        klasseanmeldenregestrieren benutzer;
        bool erraten = false;
        int anzahl = 0;

        Random r = new Random();



        int zz; 

       
        public frm_zahlenraten(int id, string benutzername, string passwort, string farbe, int punkteball, int punkte4gewinnt, int gegnerpunkte, int versuche)
        {
            InitializeComponent();
            benutzer = new klasseanmeldenregestrieren(id, benutzername, passwort, farbe, punkteball, punkte4gewinnt, gegnerpunkte, versuche);
        }
        private void frm_zahlenraten_Load(object sender, EventArgs e)
        {          
          zz  = r.Next(1, 100);
        }
        private void btn_einloggen_Click(object sender, EventArgs e)
        {
            try
            {
                int zahl = Convert.ToInt32(txt_schaetzung.Text);
                if (zahl == zz)
                    {
                        txt_schaetzung.BackColor = Color.Green;
                        MessageBox.Show("Sie haben mit " + anzahl + " Versuchen die Zahl erraten");

                        erraten = true;
                        clsDataProvider.Zahlenraten(anzahl, benutzer.Id);

                        DialogResult = DialogResult.OK;
                    }
                    else
                    {
                        txt_schaetzung.BackColor = Color.Red;
                    
                        if (zahl > zz)
                        {
                            tipp = "Die Zahl ist kleiner als dein eingegebener Wert";
                        }
                        else if (zahl < zz)
                        {
                            tipp = "Die Zahl ist größer als dein eingegebener Wert";
                        }

                        MessageBox.Show("Falsch versuchen sie es erneut oder verwenden sie einen Tipp");
                        
                    }
                    anzahl++;
               
                    lbl_anzahlversuche.Text = Convert.ToString(anzahl);
                   
                
                if (anzahl > 9)
                {
                    MessageBox.Show("Sie haben zu viele verusche verwendet probieren sie es das nächste mal erneut");
                    benutzer.Versuche = 0;
                }

            }
            catch
            {
                MessageBox.Show("Etwas ist schief gelaufen");
            }

        }
       
        private void btn_tipp_Click(object sender, EventArgs e)
        {
            lbl_tipp.Text = tipp;
        }
    }
}
