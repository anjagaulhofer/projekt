﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Semesterprojekt_Versuch_2_2024
{
    public partial class frm_farbauswahl : Form
    {
        klasseanmeldenregestrieren benutzer;
        bool akt;
        public frm_farbauswahl()
        {
            InitializeComponent();
        }
      
        public frm_farbauswahl(int id, string benutzername, string passwort, string farbe, int punkteball, int punkte4gewinnt, int gegnerpunkte,int versuche)
        {
            InitializeComponent();
            benutzer = new klasseanmeldenregestrieren(id, benutzername, passwort, farbe, punkteball, punkte4gewinnt, gegnerpunkte,versuche);
            
        }

        private void btn_farbeuebernehmen_Click(object sender, EventArgs e)
        {
          
            try
            {

                benutzer.Farbe = cb_farben.Text;


                if (benutzer.Farbe == "Blau")
                {

                    btn_color.BackColor = Color.Blue;
                }
                else if (benutzer.Farbe == "Grün")
                {
                    btn_color.BackColor = Color.Green;
                }
                else if (benutzer.Farbe == "Gelb")
                {
                    btn_color.BackColor = Color.Yellow;
                }
                else if (benutzer.Farbe == "Orange")
                {
                    btn_color.BackColor = Color.Orange;
                }
                else if (benutzer.Farbe == "Rot")
                {
                    btn_color.BackColor = Color.Red;
                }
                else if (benutzer.Farbe == "Rosa")
                {
                    btn_color.BackColor = Color.Pink;
                }
                else if (benutzer.Farbe == "Violet")
                {
                    btn_color.BackColor = Color.Violet;
                }
                clsDataProvider.InsertFarbe(benutzer.Farbe, benutzer.Id);
              
                DialogResult = DialogResult.OK;


            }
            catch
            {
                MessageBox.Show("Versuchen Sie es erneut");
            }
        }

        private void frm_farbauswahl_Load(object sender, EventArgs e)
        {
            try
            {

                if (benutzer.Farbe == "Blau")
                {

                    btn_color.BackColor = Color.Blue;
                    cb_farben.Text = "Blau";
                }
                else if (benutzer.Farbe == "Grün")
                {
                    btn_color.BackColor = Color.Green;
                    cb_farben.Text = "Grün";
                }
                else if (benutzer.Farbe == "Gelb")
                {
                    btn_color.BackColor = Color.Yellow;
                    cb_farben.Text = "Gelb";
                }
                else if (benutzer.Farbe == "Orange")
                {
                    btn_color.BackColor = Color.Orange;
                    cb_farben.Text = "Orange";
                }
                else if (benutzer.Farbe == "Rot")
                {
                    btn_color.BackColor = Color.Red;
                    cb_farben.Text = "Rot";
                }
                else if (benutzer.Farbe == "Rosa")
                {
                    btn_color.BackColor = Color.Pink;
                    cb_farben.Text = "Rosa";
                }
                else if (benutzer.Farbe == "Violet")
                {
                    btn_color.BackColor = Color.Violet;
                    cb_farben.Text = "Violet";
                }


                clsDataProvider.InsertFarbe(benutzer.Farbe, benutzer.Id);
            }
            catch
            {
                MessageBox.Show("Versuchen Sie es erneut");
            }
        }

        private void btn_color_Click(object sender, EventArgs e)
        {
            cd_color.ShowDialog();
            Color farbfeld = cd_color.Color;
            btn_color.BackColor = cd_color.Color;
            string farbe = Convert.ToString(btn_color.BackColor);
            clsDataProvider.InsertFarbe(benutzer.Farbe, benutzer.Id);
            

        }

        private void cb_farben_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                benutzer.Farbe = cb_farben.Text;


                if (benutzer.Farbe == "Blau")
                {

                    btn_color.BackColor = Color.Blue;
                }
                else if (benutzer.Farbe == "Grün")
                {
                    btn_color.BackColor = Color.Green;
                }
                else if (benutzer.Farbe == "Gelb")
                {
                    btn_color.BackColor = Color.Yellow;
                }
                else if (benutzer.Farbe == "Orange")
                {
                    btn_color.BackColor = Color.Orange;
                }
                else if (benutzer.Farbe == "Rot")
                {
                    btn_color.BackColor = Color.Red;
                }
                else if (benutzer.Farbe == "Rosa")
                {
                    btn_color.BackColor = Color.Pink;
                }
                else if (benutzer.Farbe == "Violet")
                {
                    btn_color.BackColor = Color.Violet;
                }
                clsDataProvider.InsertFarbe(benutzer.Farbe, benutzer.Id);



            }
            catch
            {
                MessageBox.Show("Versuchen Sie es erneut");
            }
        }

        private void frm_farbauswahl_FormClosed(object sender, FormClosedEventArgs e)
        {
            akt = true;
        }
    }




  

        
      
    
    }

